<div class="grid-container-top">
        <div class="row">
            <div class="col-3">
                <a href="shop.html">
                    <img src="logo.png" alt="" class="logo" style="width:100px;height: 100;">
                </a>
            </div>
            <div class="col-2">
                <section class="lefttext">
                    <h6><b>Send us a message</b></h6><br>
                    <p class="mail">demo@demo.com</p>
                </section>
            </div>
            <div class="col-4">
                <section class="righttext">
                    <h6>Need Help? Call us:<br>
                        <b class="number">012 345 6789</b>
                    </h6>
                </section>
            </div>
            <div class="col-3">
                <section class="icons">
                    <i class="fa fa-user"></i>
                    <i class="fa fa-heart"></i>
                    <i class="fa fa-shopping-bag"></i>
                </section>
            </div>
        </div>
    </div>
    <hr>
    <nav>
