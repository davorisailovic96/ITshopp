<?php
require_once 'db.php';

class DAO {
	private $db;

	// za 2. nacin resenja
	/*private $INSERT_AUTOMOBILI = "INSERT INTO automobili (id_marke, cena, godiste,danKupovine) VALUES (?,?, ?, ?)";
	private $UPDATE_AUTOMOBILI = "UPDATE automobili SET id_marke = ?, cena = ?, godiste = ?,danKupovine = ? WHERE id_automobila = ?";
	private $DELETE_AUTOMOBIL = "DELETE  FROM automobili WHERE id_automobila = ?";
	private $SELECT_AUTOMOBIL_BY_ID = "SELECT * FROM automobili WHERE id_automobila = ?";	
	private $SELECT_AUTOMOBILI = "SELECT * FROM automobili JOIN marke on automobili.id_marke = marke.id_marke";
	private $SELECT_AUTOMOBIL_BY_DANKUPOVINE = "SELECT * FROM automobili where danKUpovine = ?";
	private $SELECT_AUTOMOBIL_BY_GODISTE = "SELECT * FROM automobili where godiste = ?";
	private $SHOW_AO_BY_MARKA = "SELECT o.ime, o.prezime, ao.vreme_zaduzenja, a.id_automobila, m.marka FROM (((osobe o JOIN automobili_osobe ao ON o.id_osobe=ao.id_osoba) JOIN automobili a ON a.id_automobila=ao.id_automobil) JOIN marke m ON m.id_marke=a.id_marke)
	WHERE m.marka = '?';";*/
	private $select_by_type = "SELECT * FROM product where product_id= ?";
	private $select_all = "SELECT * FROM product order by type_id";
	private $show_filters = "SELECT t.type, b.brand FROM (product p join type_of_product t on t.type_id=p.type_id) join brand b on b.brand_id=p.brand_id";
	private $SELECT_USER_BY_USERNAME= "SELECT * FROM user";
	
	public function __construct()
	{
		$this->db = DB::createInstance();
	}
	
	/*public function selectAutomobili()
	{
		
		$statement = $this->db->prepare($this->SELECT_AUTOMOBILI);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		//var_dump($result);
		return $result;
	}

	
	public function insertAutomobil($marka, $cena, $godiste, $danKupovine)
	{
		
		$statement = $this->db->prepare($this->INSERT_AUTOMOBILI);
		$statement->bindValue(1, $marka);
		$statement->bindValue(2, $cena);
		$statement->bindValue(3, $godiste);
		$statement->bindValue(4, $danKupovine);
		
		$statement->execute();
	}

	public function updateAutomobil($marka, $cena, $godiste, $danKupovine, $id)
	{
		
		$statement = $this->db->prepare($this->UPDATE_AUTOMOBILI);
		$statement->bindValue(1, $marka);
		$statement->bindValue(2, $cena);
		$statement->bindValue(3, $godiste);
		$statement->bindValue(4, $danKupovine);
		$statement->bindValue(5, $id);
		
		$statement->execute();
	}

	public function deleteAutomobilById($id)
	{
		$statement = $this->db->prepare($this->DELETE_AUTOMOBIL);
		$statement->bindValue(1, $id);
		
		$statement->execute();
	}

	public function selectAautomobiliByDanKupovine($danKupovine)
	{
		$statement = $this->db->prepare($this->SELECT_AUTOMOBIL_BY_DANKUPOVINE);
		$statement->bindValue(1, $danKupovine);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	public function selectAautomobiliByGodiste($godiste)
	{
		$statement = $this->db->prepare($this->SELECT_AUTOMOBIL_BY_GODISTE);
		$statement->bindValue(1, $godiste);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	public function selectAutomobilByID($id)
	{
		$statement = $this->db->prepare($this->SELECT_AUTOMOBIL_BY_ID);
		$statement->bindValue(1, $id);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
	public function show_ao_by_marka($marka)
	{
		$statement = $this->db->prepare($this->SHOW_AO_BY_MARKA);
		$statement->bindValue(1, $marka);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}*/
	public function show_by_type($type)
	{
		
		$statement = $this->db->prepare($this->select_by_type);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		//var_dump($result);
		return $result;
	}
	public function show_all()
	{
		
		$statement = $this->db->prepare($this->select_all);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		//var_dump($result);
		return $result;
	}
	public function show_filters()
    {
		$statement = $this->db->prepare($this->show_filters);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		//var_dump($result);
		return $result;

        
    }
	public function selectUserByUsername($username)
	{
		
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME);
		$statement->bindValue(1, $username);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}

}
?>
